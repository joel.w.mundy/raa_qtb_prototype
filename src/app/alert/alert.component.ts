import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input() type: String;
  @Input() heading: String;
  @Input() subheading: String;

  constructor() { }

  ngOnInit(): void {
  }

}
