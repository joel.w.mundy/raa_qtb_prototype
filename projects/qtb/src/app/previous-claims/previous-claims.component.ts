import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Driver, Claim } from '../core/models/quote-request.model';
import { FormOptionsService } from '../form-options.service';
import { NgModel, NgForm } from '@angular/forms';

@Component({
  selector: 'qtb-previous-claims',
  templateUrl: './previous-claims.component.html',
  styleUrls: ['./previous-claims.component.scss']
})
export class PreviousClaimsComponent implements OnInit {

  @Input() driver: Driver;
  @ViewChild('newClaim') newClaim: NgForm;
  model: Claim = {
    type: '',
    year: null,
    month: '',
  }
  constructor(public options: FormOptionsService) { }

  ngOnInit(): void {
  }
  edit(i: number) {
    const claim = Object.assign({}, this.driver.claims[i]);
    this.driver.claims.splice(i, 1);
    this.model = Object.assign({}, claim);
  }
  remove(i: number) {
    this.driver.claims.splice(i, 1);
  }
  add() {
    if (!this.newClaim.valid) {
      return;
    }
    const newClaim = Object.assign({}, this.newClaim.value);
    if (!this.driver.claims) {
      this.driver.claims = [];
    }
    this.driver.claims.push(newClaim);
    this.newClaim.reset();
    console.log('something');
  }

}
