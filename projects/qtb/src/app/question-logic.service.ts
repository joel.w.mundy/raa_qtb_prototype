import { Injectable } from '@angular/core';
import { FormService } from './form.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionLogicService {
  get askBusinessUse(): boolean {
    if (!this.formModel || !this.formModel.vehicle || !this.formModel.vehicle.CarUsage) {
      return false;
    }
    const carUsage = this.formModel.vehicle.CarUsage;
    if (carUsage.toLowerCase() === 'business') {
      return true;
    }
    return false;
  }
  get displayOtherBusinesses(): boolean {
    if (!this.formModel.vehicle.businessType) {
      return false;
    }
    return this.formModel.vehicle.businessType.toLowerCase() === 'other';
  }
  get isRestrictedBusinessType(): boolean {
    if (!this.formModel.vehicle.otherBusinessType || this.formModel.vehicle.businessType.toLowerCase() !== 'other') {
      return false;
    }
    const restrictedBusinesses = [
      'taxi', 'security', 'courtesy', 'food', 'instructorOther', 'instructorRaa'
    ].map(x => x.toLowerCase());
    const selectedOtherBusiness = this.formModel.vehicle.otherBusinessType.toLowerCase();
    return restrictedBusinesses.indexOf(selectedOtherBusiness) >= 0;
  }
  get callApplicant(): boolean {
    if (!this.formModel.vehicle.otherBusinessType || this.formModel.vehicle.businessType.toLowerCase() !== 'other') {
      return false;
    }
    const callApplicant = [
      'adultIndustry', 'hunter', 'courtesy'
    ].map(x => x.toLowerCase());
    const selectedOtherBusiness = this.formModel.vehicle.otherBusinessType.toLowerCase();
    return callApplicant.indexOf(selectedOtherBusiness) >= 0;
  }
  get emmaDistanceText(): string {
    if (!this.formModel.vehicle.distance) {
      return "";
    }
    const perWeek = Math.floor(Number(this.formModel.vehicle.distance) / 52);
    return `That's about ${perWeek} km per week`;
  }
  get carBoughtInLast3Months(): boolean {
    if (!this.formModel || !this.formModel.vehicle || !this.formModel.vehicle.last3Months) {
      return false;
    }
    const wasBoughtInLast3Months = this.formModel.vehicle.last3Months.toLowerCase() === 'true';
    if (wasBoughtInLast3Months) {
      return true;
    } else {
      return false;
    }
  }
  get last3MonthsIsLastQuestion(): boolean {
    if (!this.formModel || !this.formModel.vehicle || !this.formModel.vehicle.last3Months) {
      return false;
    }
    const wasNotBoughtInLast3Months = this.formModel.vehicle.last3Months.toLowerCase() === 'false';
    return wasNotBoughtInLast3Months;
  }
  get askIfCarWasBrandNew(): boolean {
    return this.carBoughtInLast3Months;
  }
  get askIfCarFirstPolicy(): boolean {
    if (!this.formModel || !this.formModel.vehicle || !this.formModel.vehicle.newCar) {
      return false;
    }
    const isNewCar = this.formModel.vehicle.newCar.toLowerCase() === 'true';
    return isNewCar;
  }
  get askIfCarFinanced(): boolean {
    return this.carBoughtInLast3Months;
  }
  get askFinanceProvider(): boolean {
    if (!this.formModel || !this.formModel.vehicle || !this.formModel.vehicle.financed) {
      return false;
    }
    const isFinanced = this.formModel.vehicle.financed.toLowerCase() === 'true';
    return isFinanced;
  }
  isSenior(age: number) {
    if (age >= 60) {
      return true;
    }
    return false;
  }

  constructor(private formModel: FormService) { }
}
