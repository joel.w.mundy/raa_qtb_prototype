import { Injectable } from '@angular/core';
import { selectOption } from './core/models/interfaces';
import { FormService } from './form.service';

@Injectable({
  providedIn: 'root'
})
export class FormOptionsService {
  carUseTypes: selectOption[] = [
    {display: 'Private', value: 'Private'},
    {display: 'Business', value: 'Business'},
  ];
  businessTypes: selectOption[] = [
    {display: 'Trade e.g. plumber, builder etc.', value: 'trade'},
    {display: 'Consultant', value: 'Consultant' },
    {display: 'Representative', value: 'Representative' },
    {display: 'Rideshare', value: 'Rideshare' },
    {display: 'District nurse', value: 'District' },
    {display: 'Other', value: 'Other' }
  ];
  otherBusinesses: selectOption[] = [
    { display: 'Taxi/Courier/Delivery', value: 'taxi' },
    { display: 'Collectors', value: 'collector'},
    { display: 'Agents', value: 'agent'},
    { display: 'Technicians', value: 'technician'},
    { display: 'Business - other', value: 'other'},
    { display: 'Driving instructor - other', value: 'instructorOther'},
    { display: 'Driving instructor - RAA', value: 'instructorRaa'},
    { display: 'Photographer', value: 'photographer'},
    { display: 'Pest control', value: 'pest'},
    { display: 'Mobile mechanic', value: 'mechanic'},
    { display: 'House/carpet/window cleaning', value: 'cleaner'},
    { display: 'Landscaping/lawn moving/paving', value: 'landscaping'},
    { display: 'Professional hunter', value: 'hunter'},
    { display: 'Associated with the sex inductry', value: 'adultIndustry'},
    { display: 'Food/refridgerated van', value: 'food'},
    { display: 'Courtesy vehicle', value: 'courtesy'},
    { display: 'Security patrol', value: 'security'},
  ]
    travelDistances: selectOption[] = [
      {display: 'Less than 5,000km', value: '5000'},
      {display: '5,000km - 10,000km', value: '10000'},
      {display: '10,000km - 15,000km', value: '15000'},
      {display: '15,000km - 20,000km', value: '20000'},
      {display: '20,000km - 40,000km', value: '40000'},
      {display: 'over 40,000km', value: '50000'},
    ];
    coverTypes: selectOption[] = [
      {display: 'garage', value: 'garage'},
      {display: 'carport', value: 'carport'},
      {display: 'driveway', value: 'driveway'},
      {display: 'street', value: 'street'},
      {display: 'residential', value: 'residential'},
      {display: 'other', value: 'other'}
    ];
    yesOrNo: selectOption[] = [
      {display: 'Yes', value: true},
      {display: 'No', value: false},
    ];
    carResults: selectOption[] = [
      {display: 'Mazda CX-5 MAXX Sport (4x4) MY19', value: '111111'},
      {display: 'Mazda CX-5 MAXX Sport (4x4) MY19 (KF Series 2)', value: '22222'}
    ];
    titles: selectOption[] = [
      {display: 'Mr', value: 'mr'},
      {display: 'Mrs', value: 'mrs'},
      {display: 'Miss', value: 'miss'},
      {display: 'Ms', value: 'ms'},
      {display: 'Other', value: 'other'},
    ];
    genders: selectOption[] = [
      {display: 'Male', value: 'male'},
      {display: 'Female', value: 'female'}
    ];
    // drivers: selectOption[] = [
    //   {display: 'Male', value: 'male'},
    //   {display: 'Female', value: 'female'}
    // ];
    driverTypes = [
      {display: 'Main driver', value: 'Main driver'},
      {display: 'Frequent driver', value: 'Frequent driver'},
      {display: 'Infrequent driver', value: 'Infrequent driver'},
      {display: 'Never drives the car', value: 'Never drives the car'},
    ];
    financeProviders = [
      {display: 'RAA Car Finance', value: 'raa'},
      {display: 'People\'s Choice Car Loans', value: 'pccu'},
      {display: 'Canstar', value: 'canstar'}
    ];
    claimTypes = [
      {display: 'Accidental damage', value: 'accidental'},
      {display: 'Collision', value: 'collision'},
      {display: 'Fire', value: 'fire'},
      {display: 'Impact', value: 'impact'},
      {display: 'Mallicious damage', value: 'malicious'},
      {display: 'Storm/hail/flood', value: 'weather'},
      {display: 'Theft', value: 'theft'}
    ];
    claimYears = [
      {display: '2020', value: '2020'},
      {display: '2019', value: '2019'},
      {display: '2018', value: '2018'},
      {display: '2017', value: '2017'},
      {display: '2016', value: '2016'},
    ];
    months = [
      {display: 'January', value: 'January'},
      {display: 'February', value: 'February'},
      {display: 'March', value: 'March'},
      {display: 'April', value: 'April'},
      {display: 'May', value: 'May'},
      {display: 'June', value: 'June'},
      {display: 'July', value: 'July'},
      {display: 'August', value: 'August'},
      {display: 'September', value: 'September'},
      {display: 'October', value: 'October'},
      {display: 'November', value: 'November'},
      {display: 'December', value: 'December'},
    ];
    

    setDrivers(): void {
      const drivers = this.formModel.drivers;
      const driverOptions = drivers.map(x => {
        const fullName = x.Firstname + ' ' + x.Lastname;
        return {display: fullName, value: fullName};
      });
      this.drivers = driverOptions;
    }
    drivers: selectOption[];
  constructor(private formModel: FormService) { }
}
