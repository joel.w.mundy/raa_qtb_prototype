export interface QuoteRequest {
  primaryContact?: string;
  selectedQuote?: string;
  MetaData?: {
    IPAddress: string;
    UserAgent: string;
    QuoteType: string;
  };
  QuoteDate?: string;
  WSQuoteRef?: string;
  SaveQuote?: string;
  SaveResponse?: string;
  QuoteSettings?: {
    QuoteSchemes: {
      Number: string[];
    };
  };
  PolicyStartDate?: string;
  HireCarOption?: string;
  CriminalConviction?: string;
  DutyOfDisclosure?: string;
  InsuranceDeclinedInPastXYears?: string;
  ConfirmedAccidentStatus?: string;
  OptionalExtras?: {
    OptionalExtra: {
      ExtraType: string;
    }[];
  };
  Drivers?: {
    Driver: Driver[];
  };
  OtherDrivers?: {
    Driver: Driver[];
  };
  PartialOtherDriver?: Driver;
  Vehicles?: {
    Vehicle: Vehicle[];
  };
}

export interface Driver {

  usage?: string; // how often
  addOthers?: string; // how often
  isSenior?: string;
  claims?: Claim[];

  Title?: string;
  titleOther?: string;
  Firstname?: string;
  Lastname?: string;
  DOB?: string;
  DeclaredMembership?: string;
  Gender?: string;
  AgeLicenseObtained?: string;
  OwnThisVehicle?: string;
  OwnAnotherVehicle?: string;
  DriverAddress?: string;
  DriverType?: string;
  COMPforMainDriverInLastYear?: string;
  PreviousInsurerforCOMP?: string;
  NCB?: string;
  IsClient?: string;
  IsPolicyHolder?: string;
  NoAccidentsInPastXYears?: string;
  HasSeniorsCard?: string;
  MemberNumber?: string;
  Accidents?: {
    Accident: {
      AbiCode: string;
      AccidentDate: string;
    }[];
  };
}

export interface Vehicle {
  make?: string;
  model?: string;
  year?: string;
  transmission?: string;
  engine?: string;
  matchedId?: string;
  businessType?: string;
  otherBusinessType?: string;
  distance?: string;
  location?: string;
  coverType?: string;
  last3Months?: string;
  newCar?: string;
  firstPolicy?: string;
  financed?: string;

  ModelCode?: string;
  RegistrationNo?: string;
  VIN?: string;
  PurchaseDate?: string;
  PurchasePrice?: string;
  IsCarFinanced?: string;
  CarUsage?: string;
  Interests?: {
    InterestedParty: string;
  }[];
  Garaging?: {
    Postcode: string;
    Suburb: string;
    State: string;
  };
  StorageType?: string;
  AverageKmsDriven?: string;
  PreviousInsurer?: string;
  NCB?: string;
  FirstRegDate?: string;
  VoluntaryExcess?: string;
  FrequencyOfUse?: string;
  PermittedDrivers?: string;
  Ownership?: string;
  Keeper?: string;
  HasAnyDamage?: string;
  RepairCostMoreThanX?: string;
  HasHailDamage?: string;
  Modifications?: {
    Modification: {
      Code: string;
      Price: string;
      Type: string;
      IsFitted: string;
    }[];
  };
}
export interface Claim {
  type: string;
  year: number;
  month: string;
}