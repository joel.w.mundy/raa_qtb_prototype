export interface selectOption {
    value: string | boolean,
    display: string
  }

export interface QTB {
  firstName: string,
  lastName: string,
  vehicle?: {
    make: string,
    model: string,
    year: string,
    transmission: string,
    engine: string,
    matchedId: string
  }
}