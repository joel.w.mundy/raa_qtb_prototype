import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevDebugComponent } from './dev-debug.component';

describe('DevDebugComponent', () => {
  let component: DevDebugComponent;
  let fixture: ComponentFixture<DevDebugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevDebugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevDebugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
