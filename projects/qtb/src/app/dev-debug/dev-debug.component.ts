import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormService } from '../form.service';

@Component({
  selector: 'qtb-dev-debug',
  templateUrl: './dev-debug.component.html',
  styleUrls: ['./dev-debug.component.scss']
})
export class DevDebugComponent implements OnInit {
  @Input() form: any;
  show: boolean = true;
  formService: any;
  get formStep() {
    if (this.formService.stepper) {
      const selectedIndex = this.formService.stepper.selectedIndex;
      return selectedIndex;
    }
    else return 0;
  }
  constructor(
    private _formService: FormService
  ) {
    this.formService = _formService
  }

  ngOnInit(): void {
  }
  test() {
    console.log('testing dev-debug');
  }

}
