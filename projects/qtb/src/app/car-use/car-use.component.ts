import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ControlContainer, NgForm, NgModelGroup } from '@angular/forms';
import { QTB } from '../core/models/interfaces';
import { FormService } from '../form.service';
import { QuoteRequest, Vehicle, Driver } from '../core/models/quote-request.model';
import { MatStepper } from '@angular/material/stepper';
import { FormOptionsService } from '../form-options.service';
import { QuestionLogicService } from '../question-logic.service';

@Component({
  selector: 'qtb-car-use',
  templateUrl: './car-use.component.html',
  styleUrls: ['./car-use.component.scss'],
})
export class CarUseComponent implements OnInit, AfterViewInit {

    options: FormOptionsService;
    formModel: FormService;

    @ViewChild('form') public form: NgForm;
    @ViewChild('stepper') public stepper: MatStepper;

  constructor(
    private formService: FormService,
    private formOptionsService: FormOptionsService,
    public questionLogic: QuestionLogicService
  ) {
    this.options = formOptionsService;
  }
  
  ngOnInit(): void {
    this.formModel = this.formService;
  }
  ngAfterViewInit() {
    this.formService.initStepper(this.stepper);
  }
  nextStepWithDelay() {
    setTimeout(() => this.stepper.next(), 500);
  }
  test() {
    console.log('testing car-use');
  }

}
