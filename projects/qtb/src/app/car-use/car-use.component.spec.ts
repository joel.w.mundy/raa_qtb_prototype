import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarUseComponent } from './car-use.component';

describe('CarUseComponent', () => {
  let component: CarUseComponent;
  let fixture: ComponentFixture<CarUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarUseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
