import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormService } from '../form.service';
import { FormGroup, NgForm } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'qtb-stepper-nav',
  templateUrl: './stepper-nav.component.html',
  styleUrls: ['./stepper-nav.component.scss']
})
export class StepperNavComponent implements OnInit {
  @Input() showBack: any = true;
  @Input() showNext: any = true;
  @Input() nextRouterLink: any;
  @Input() previousRouterLink: any;
  @Input() nextText: string = "Next"
  @Input() backText: string = "Back";
  @Input() form: NgForm;
  @Input() preFunction: Function;
  @Input() stepper: MatStepper;
  constructor(
    private router: Router,
    private formService: FormService
  ) { }

  ngOnInit(): void {
  }
  nextQuestion() {
    const form = this.formService.formGroup;
    this.markFormGroupTouched(form.control);
  }
  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }
  next() {
    this.stepper
  }
  nextPage() {
    if (this.form.valid) {
      this.formService.saveModel();
      this.router.navigate([this.nextRouterLink], { state: this.formService.model });
    }
    else {
      // there are form errors!
    }
  }

}
