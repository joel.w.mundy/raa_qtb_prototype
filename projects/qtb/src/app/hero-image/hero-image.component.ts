import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'qtb-hero-image',
  templateUrl: './hero-image.component.html',
  styleUrls: ['./hero-image.component.scss']
})
export class HeroImageComponent implements OnInit {
  @Input() title: string;
  @Input() imageType: string = 'car';
  constructor() { }

  ngOnInit(): void {
  }

}
