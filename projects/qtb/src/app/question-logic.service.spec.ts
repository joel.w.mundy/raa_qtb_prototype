import { TestBed } from '@angular/core/testing';

import { QuestionLogicService } from './question-logic.service';

describe('QuestionLogicService', () => {
  let service: QuestionLogicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionLogicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
