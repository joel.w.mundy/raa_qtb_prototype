import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'qtb-emma',
  templateUrl: './emma.component.html',
  styleUrls: ['./emma.component.scss']
})
export class EmmaComponent implements OnInit {

  _text;
  _hide = true;

  @Input() set text(value: string) {
    if (value !== this._text) {
      this._text = value;
      this.cdr.markForCheck();
    }
  }

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }

}
