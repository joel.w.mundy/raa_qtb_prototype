import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'qtb-incrementor',
  templateUrl: './incrementor.component.html',
  styleUrls: ['./incrementor.component.scss']
})
export class IncrementorComponent implements OnInit {

  @Input() label: string;
  @Input() default: string;

  @ContentChild('inputs') inputsTmpl: TemplateRef<any>;
  constructor() { }

  ngOnInit(): void {

  }

  increment() {

  }

  decrement() {

  }
}
