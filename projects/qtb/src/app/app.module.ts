import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { DutyOfDisclosureComponent } from './duty-of-disclosure/duty-of-disclosure.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GeneralInfoComponent } from './general-info/general-info.component';
import { CarUseComponent } from './car-use/car-use.component';
import { DriversComponent } from './drivers/drivers.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { QuoteComponent } from './quote/quote.component';
import { AngularMaterialImportsModule } from './angular-material-imports.module';
import { FormsModule } from '@angular/forms';
import { DevDebugComponent } from './dev-debug/dev-debug.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { StepperNavComponent } from './stepper-nav/stepper-nav.component';
import { RadiobuttonComponent } from './radiobutton/radiobutton.component';
import { ExperimentComponent } from './experiment/experiment.component';
import { HeroImageComponent } from './hero-image/hero-image.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { AddDriversComponent } from './add-drivers/add-drivers.component';
import { QuestionHeadingComponent } from './question-heading/question-heading.component';
import { EmmaComponent } from './emma/emma.component';
import { NullOnHideDirective } from './null-on-hide.directive';
import { ErrorMessageDirective } from './error-message.directive';
import { ControlErrorComponent } from './control-error/control-error.component';
import { DisplayEmmaDirective } from './display-emma.directive';
import { SelectListComponent } from './select-list/select-list.component';
import { AlertComponent } from '../../../../src/app/alert/alert.component';
import { AdditionalDriverQuestionsComponent } from './additional-driver-questions/additional-driver-questions.component';
import { PreviousClaimsComponent } from './previous-claims/previous-claims.component';
import { IncrementorComponent } from './incrementor/incrementor.component';

const providers = [];
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DutyOfDisclosureComponent,
    PageNotFoundComponent,
    GeneralInfoComponent,
    CarUseComponent,
    DriversComponent,
    PersonalDetailsComponent,
    DevDebugComponent,
    CarDetailsComponent,
    QuoteComponent,
    StepperNavComponent,
    RadiobuttonComponent,
    ExperimentComponent,
    HeroImageComponent,
    ProgressBarComponent,
    AddDriversComponent,
    QuestionHeadingComponent,
    EmmaComponent,
    NullOnHideDirective,
    ErrorMessageDirective,
    ControlErrorComponent,
    DisplayEmmaDirective,
    SelectListComponent,
    AlertComponent,
    AdditionalDriverQuestionsComponent,
    PreviousClaimsComponent,
    IncrementorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialImportsModule,
    FormsModule
  ],
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class QtbSharedModule{
  static forRoot(): ModuleWithProviders<AppModule> {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}