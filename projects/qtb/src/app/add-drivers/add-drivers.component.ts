import { Component, OnInit, Input, Output, AfterViewInit, AfterContentChecked } from '@angular/core';
import { FormOptionsService } from '../form-options.service';
import { Driver } from '../core/models/quote-request.model';
import { FormService } from '../form.service';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'qtb-add-drivers',
  templateUrl: './add-drivers.component.html',
  styleUrls: ['./add-drivers.component.scss']
})
export class AddDriversComponent implements OnInit, AfterContentChecked {

  @Input() stepper: MatStepper;
  driverIndex: number = 0;
  options: FormOptionsService;
  @Input() drivers: Driver[];
  @Input() savePrevious: boolean;

  otherDrivers: Driver[];
  driversForDisplay: any;
  constructor(
    private formOptionsService: FormOptionsService,
    private formModel: FormService
  ) {
    this.options = formOptionsService;
  }

  setDrivers() {
    // this.otherDrivers = this.formService.otherDrivers;
    // const allDrivers = [...this.drivers, ...this.otherDrivers];
    const allDrivers = this.drivers;
    this.driversForDisplay = allDrivers.map(x => {
      const fullName = x.Firstname + (x.Lastname? ' ' + x.Lastname: '');
      let details = x.usage;
      if (x.IsClient === 'true') {
        details += ' | RAA Member';
      }
      return { name: fullName, details: details};
    });
  }
  ngOnInit(): void {
    
  }
  
  ngAfterContentChecked() {
    this.setDrivers();
  }
  radioButtonSelected(value) {
    if (value) {
      this.addOtherDriver();
    } else {
      this.saveDriver();
    }
  }
  addOtherDriver() {
    this.driverIndex++;
    // if (this.otherDrivers[this.otherDrivers.length - 1] !== {}) {
    //   this.otherDrivers.push({});
    // }
    // this.stepper.next();

    this.formModel.newDriver = {};
    this.stepper.selectedIndex = this.formModel.addDriverIndex;
  }
  saveDriver() {
    // this.removeEmpty();
    this.formOptionsService.setDrivers();
    // this.stepper.next();

    const afterAddingPeopleIndex = this.formModel.afterAddDriverIndex;
    this.setStepsCompletedUntil(afterAddingPeopleIndex);
    this.stepper.selectedIndex = afterAddingPeopleIndex;
  }
  setStepsCompletedUntil(step: number) {
    const stepArray = this.stepper.steps.toArray()
    const currentStep = this.stepper.selectedIndex;
    for(var i = currentStep; i < step; i++) {
      stepArray[i].completed = true;
    }
  }
  edit(i: number) {
    this.formModel.newDriver = Object.assign({}, this.formModel.drivers[i]);
    this.remove(i);

    // super dodgy.
    this.stepper.selectedIndex = this.formModel.addDriverIndex;
  }
  removeEmpty() {
    const emptyObjects = this.otherDrivers.map(obj => {
      const isEmpty = Object.keys(obj).length === 0 && obj.constructor === Object
      return isEmpty;
    }).filter((isEmpty, i) => {
      if (isEmpty) {
        return i;
      }
    });
    console.log(emptyObjects);
  }
  remove(i) {
    const driverLength = this.formModel.drivers.length;
    // if (driverLength === 1) { return; }
    this.formModel.drivers.splice(i, 1);
    this.setDrivers();
  }
  test() {
    console.log('testing add drivers');
  }

}
