import { Directive, Input, ComponentRef, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { EmmaComponent } from './emma/emma.component';

@Directive({
  selector: '[qtbDisplayEmma]'
})
export class DisplayEmmaDirective {
  _text: string;
  ref: ComponentRef<EmmaComponent>;
  container: ViewContainerRef;
  @Input() set emmaText(value: string) {
    if (value !== this._text) {
      this._text = value;
    }
    if (this._text) {
      this.attachEmma(this._text);
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private vcr: ViewContainerRef
  ) {
    this.container = vcr;
  }
  ngOnInit() {
    if (this._text) {
      this.attachEmma(this._text);
    }
  }

  attachEmma(text: string) {
    if (!this.ref) {
      const factory = this.resolver.resolveComponentFactory(EmmaComponent);
      this.ref = this.container.createComponent(factory);
    }
    this.ref.instance.text = text;
  }

}
