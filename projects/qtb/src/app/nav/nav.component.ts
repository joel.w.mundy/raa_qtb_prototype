import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormService } from '../form.service';

@Component({
  selector: 'qtb-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  currentIndex = 0;
  constructor(
      private router: Router,
      private formService: FormService
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        console.log(event.url);
        const url = event.url;
        const paths = this.formSteps.map(x => x.path)
        for (let i = 0; i < paths.length; i++) {
          if (url.includes(paths[i])) {
            this.currentIndex = i;
          }
        }
        console.log('currentIndex: ', this.currentIndex);
      }
    })
  }
  formSteps = [
    {path: "general-info", display: 'General info', hasSteps: true},
    {path: "car-use", display: 'Car use', hasSteps: true},
    {path: "drivers", display: 'Drivers', hasSteps: true},
    {path: "your-quote", display: 'Your quote', hasSteps: false},
  ];
  get progress(): number {
    return this.formService.getProgress();
  }
  get numSteps(): number {
    return this.formService.stepper.steps.length;
  }
  // progress(i): number {
  //   if (this.currentIndex < i) {
  //     return 0;
  //   }
  //   else if (this.currentIndex > i) {
  //     return 100;
  //   }
  //   else {
  //     return this.formService.getProgress();
  //   }
  // }
  // sectionComplete(i): boolean {
  //   if (i < this.currentIndex) {
  //     return true;
  //   }
  //   if (i === this.currentIndex) {
  //     return true;
  //   }
  //   if (i > this.currentIndex) {
  //     return false;
  //   }
  // }

  ngOnInit(): void {
  }
}
