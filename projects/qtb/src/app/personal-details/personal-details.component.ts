import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Driver } from '../core/models/quote-request.model';
import { FormOptionsService } from '../form-options.service';
import { QuestionLogicService } from '../question-logic.service';
import { NgModel, ControlContainer, NgModelGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'qtb-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class PersonalDetailsComponent implements OnInit {
  options: FormOptionsService;
  @Input() person: Driver;
  @Input() label: string = 'Can you please tell me a bit more about yourself so I can add you to the policy?';
  @ViewChild('dobControl') dobControl: NgModel;
  currentYear: number;
  currentMonth: number;
  currentDay: number;
  get personAge(): number {
    if (!this.dobControl || !this.dobControl.value) {
      return 10;
    }
    return this.dobControl.value;
  }
  isSenior(age) {
    const birthDate = new Date(age);
    const birthYear = birthDate.getFullYear();
    const birthMonth = birthDate.getMonth() + 1;
    const birthDay = birthDate.getDate() + 1;

    let yearDiff = this.currentYear - birthYear - 1;
    if (birthMonth <= this.currentMonth) {
      const birthdayEarlierThisMonth = birthMonth === this.currentMonth && birthDay <= this.currentDay;
      const birthdayEarlierThisYear = birthMonth < this.currentMonth;
      if (birthdayEarlierThisMonth || birthdayEarlierThisYear) {
        yearDiff++;
      }
    }
    if (yearDiff >= 60) {
      return true;
    }
    return false;
  }
  constructor(
    private formOptionsService: FormOptionsService,
    public questionLogic: QuestionLogicService
  ) {
    this.options = formOptionsService;
    const today = new Date();
    this.currentYear = today.getFullYear();
    this.currentMonth = today.getMonth() + 1;
    this.currentDay = today.getDate() + 1;
  }

  ngOnInit(): void {
  }

}
