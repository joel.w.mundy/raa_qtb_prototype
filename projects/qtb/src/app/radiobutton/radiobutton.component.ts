import { Component, OnInit, Input, ContentChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'qtb-radiobutton',
  templateUrl: './radiobutton.component.html',
  styleUrls: ['./radiobutton.component.scss']
})
export class RadiobuttonComponent implements OnInit {
  @Input() options: any;
  @Input() size: string = "l";

  sizeClass = "medium";

  @ContentChild('inputs') inputsTmpl: TemplateRef<any>;
  constructor() { }

  ngOnInit(): void {
    if (this.size === "s") {
      this.sizeClass = "small";
    } else if (this.size === "m") {
      this.sizeClass = "medium";
    } else if (this.size === "l") {
      this.sizeClass = "large";
    } else if (this.size == "title") {
      this.sizeClass = "title";
    } else if (this.size === 'n') {
      this.sizeClass = "none"
    } else {
      this.sizeClass = "medium";
    }
  }

}
