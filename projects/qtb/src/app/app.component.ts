import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
}]
})
export class AppComponent {
  @ViewChild('form') public form: NgForm;
  isLinear = true;
  test() {
    console.log('hello');
  }
}
