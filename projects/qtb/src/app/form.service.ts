import { Injectable } from '@angular/core';
import { QTB } from './core/models/interfaces';
import { QuoteRequest, Driver, Vehicle } from './core/models/quote-request.model';
import { MatStepper } from '@angular/material/stepper';
import { FormGroup, NgForm } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  // super dodgy
  readonly addDriverIndex = 5;
  readonly afterAddDriverIndex = 10;

  driverIndex: number = 0;
  formGroup: NgForm;
  checkingQuestion: boolean = false;
  public model: QuoteRequest;
  get savedModel(): QuoteRequest {
    return this._savedModel;
  }
  set savedModel(val: QuoteRequest) {
    console.log('in the setter');
    this._savedModel = val;
  }
  private _savedModel: QuoteRequest = {
    Drivers: {
      Driver: [
        {
          "Title": "mr",
          "Firstname": "Miles",
          "Lastname": "Davis",
          "usage": "Main driver",
          "DOB": "1930-10-09T14:30:00.000Z",
          "Gender": "male",
          "DriverAddress": "Los Angeles",
          "isSenior": "false",
          "IsClient": "false"
        },
        {
          "Title": "mrs",
          "Firstname": "Ella",
          "Lastname": "Fitzgerald",
          "DOB": "1970-10-09T14:30:00.000Z",
          "Gender": "female",
          "DriverAddress": "Melbourne",
          "IsClient": "true",
          "usage": "Main driver"
        },
        {
          "Title": "mr",
          "Firstname": "Bill",
          "Lastname": "Frisell",
          "DOB": "1920-10-09T14:30:00.000Z",
          "Gender": "male",
          "DriverAddress": "Nashville",
          "isSenior": "false",
          "IsClient": "false",
          "usage": "Infrequent driver"
        }
      ]
    },
    Vehicles: {
      Vehicle: [
        {
          make: 'CHRYSLER',
          year: '',
          transmission: '',
          engine: '',
          matchedId: '',
          CarUsage: 'something',
          businessType: ''
        }
      ]
    },
    OtherDrivers: {
      Driver: []
    },
    PartialOtherDriver: {

    }
  };
  drivers: Driver[];
  get mainDriver(): Driver {
    return this.model.Drivers.Driver[0];
  }
  otherDrivers: Driver[]
  get vehicle(): Vehicle {
    return this.model.Vehicles.Vehicle[0];
  }
  newDriver: Driver;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    ) {
    this.updateModelFromSaved();
    this.model = this.savedModel;
    this.drivers = this.model.Drivers.Driver;
    this.otherDrivers = this.model.OtherDrivers.Driver;
    this.newDriver = this.model.PartialOtherDriver;

    this.subscribeToNav();
  }
  subscribeToNav() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        // this.model = this.savedModel;
        const state = this.router.getCurrentNavigation().extras.state;
        if (state) {
          this.model = Object.assign({}, state);
        }
      }
    });
  }

  progress: number = 0;
  stepper: MatStepper;

  setProgress(currentStep: number) {
    console.log('setting progress');
    const totalSteps = this.stepper.steps.length;
    this.progress = currentStep / totalSteps * 100;
  }
  getProgress(): number {
    return this.progress;
  }
  initStepper(stepper: MatStepper) {
    this.stepper = stepper;
    this.progress = 0;
    this.stepper.selectionChange.subscribe(step => {
      this.setProgress(step.selectedIndex);
    })
  }
  saveModel():void {
    this.savedModel = this.model;
    console.log('hsdflkj');
  }
  updateModelFromSaved(): void {
    this.model = this.savedModel;
  }
}
