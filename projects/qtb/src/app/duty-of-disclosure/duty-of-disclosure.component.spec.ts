import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyOfDisclosureComponent } from './duty-of-disclosure.component';

describe('DutyOfDisclosureComponent', () => {
  let component: DutyOfDisclosureComponent;
  let fixture: ComponentFixture<DutyOfDisclosureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyOfDisclosureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyOfDisclosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
