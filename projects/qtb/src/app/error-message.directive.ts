import { Directive, ElementRef, TemplateRef, ComponentRef, ComponentFactoryResolver, ViewContainerRef, Input, Optional, ChangeDetectorRef } from '@angular/core';
import { NgModel, NgControl, NgForm } from '@angular/forms';
import { ControlErrorComponent } from './control-error/control-error.component';
import { FormService } from './form.service';

@Directive({
  selector: '[qtbErrorMessage]'
})
export class ErrorMessageDirective {
  ref: ComponentRef<ControlErrorComponent>;
  container: ViewContainerRef;
  form: NgForm;
  @Input() errorMessage: string = "this field is required";
  _control: NgModel;
  _userForm: NgModel;
  @Input() set controlName(control: NgModel) {
    if (control && control !== this._control) {
      this._control = control;
      this.initSubscribers();
      this.cdr.detectChanges();
      this.cdr.markForCheck();
    }
  }
  
  constructor(
    @Optional() private control: NgModel,
    private elementRef: ElementRef,
    private resolver: ComponentFactoryResolver,
    private vcr: ViewContainerRef,
    private formService: FormService,
    private cdr: ChangeDetectorRef
    ) {
    this.container = vcr;
    if (control) {
      this._control = control;
    }
  }
  ngOnInit() {
  }
  ngAfterContentChecked() {
    if (this._control) {
      this.initSubscribers();
    }
  }
  initSubscribers() {
    this.form = this._control.formDirective;
      this.form.ngSubmit.subscribe(() => {
        this.checkForErrors();
      });
      this._control.valueChanges.subscribe(val => {
        this.checkForErrors();
      });
  }
  checkForErrors() {
    const errors = this.displayError(this._control);
    if (errors) {
      this.setError(this.errorMessage);
    } else if (this.ref) {
      this.setError(null);
    }
  }
  setError(text: string) {
    if (!this.ref) {
      const factory = this.resolver.resolveComponentFactory(ControlErrorComponent);
      this.ref = this.container.createComponent(factory);
    }

    this.ref.instance.text = text;
  }

  displayError(control: NgModel) {
    const displayError = control.errors && (control.touched || this.form.submitted);
    return displayError;
  }

}
