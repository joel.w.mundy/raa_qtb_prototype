import { Component, OnInit } from '@angular/core';
import { FormService } from '../form.service';
import { QuoteRequest, Driver } from '../core/models/quote-request.model';

@Component({
  selector: 'qtb-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {

  model: QuoteRequest;
  mainDriver: Driver;
  formModel: FormService;
  quoteDetails = [
    {
      type: "Comprehensive",
      priceYear: 1179,
      priceMonth: 98,
      info: "Info about the quote"
    },
    {
      type: "Fire & Theft",
      priceYear: 1179,
      priceMonth: 48,
      info: "Info about the quote"
    },
    {
      type: "Third Party",
      priceYear: 1179,
      priceMonth: 45,
      info: "Info about the quote"
    }
];

excesses = [
  {
    amount: 400
  },
  {
    amount: 500
  },
  {
    amount: 600
  },
  {
    amount: 700
  },
  {
    amount: 1000
  },
  {
    amount: 1200
  }
];

  constructor(
    private formService: FormService
  ) {
    this.formModel = formService;
    // this.model = this.formService.model;
    // this.mainDriver = this.model.Drivers.Driver[0];
  }

  ngOnInit(): void {
  }
test() {
  console.log('testing');
}
}
