import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalDriverQuestionsComponent } from './additional-driver-questions.component';

describe('AdditionalDriverQuestionsComponent', () => {
  let component: AdditionalDriverQuestionsComponent;
  let fixture: ComponentFixture<AdditionalDriverQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalDriverQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalDriverQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
