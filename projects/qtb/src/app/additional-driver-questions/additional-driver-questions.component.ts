import { Component, OnInit, Input } from '@angular/core';
import { FormService } from '../form.service';
import { Driver } from '../core/models/quote-request.model';
import { FormOptionsService } from '../form-options.service';

@Component({
  selector: 'qtb-additional-driver-questions',
  templateUrl: './additional-driver-questions.component.html',
  styleUrls: ['./additional-driver-questions.component.scss']
})
export class AdditionalDriverQuestionsComponent implements OnInit {

  driver: Driver;

  constructor(
    private formService: FormService,
    public options: FormOptionsService) {
    this.driver = formService.model.PartialOtherDriver;
    this.driver = {};
  }

  ngOnInit(): void {
  }

}
