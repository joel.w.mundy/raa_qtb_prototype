import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'qtb-question-heading',
  templateUrl: './question-heading.component.html',
  styleUrls: ['./question-heading.component.scss']
})
export class QuestionHeadingComponent implements OnInit {

  @Input() heading: string;
  @Input() subHeading: string;
  @Input() moreInfoText: string;
  constructor() { }

  ngOnInit(): void {
  }

}
