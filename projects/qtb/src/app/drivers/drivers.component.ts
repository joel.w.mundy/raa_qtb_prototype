import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormService } from '../form.service';
import { MatStepper, MatStep } from '@angular/material/stepper';
import { Driver, QuoteRequest } from '../core/models/quote-request.model';
import { FormOptionsService } from '../form-options.service';

@Component({
  selector: 'qtb-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit, AfterViewInit {
  options: FormOptionsService;
  formModel: FormService;
  @ViewChild('stepper') public stepper: MatStepper;
  @ViewChild('newPersonStep') public newPersonStep: MatStep;
  
  constructor(
    private _form: FormService,
    private formOptionsService: FormOptionsService
  ) {
    this.options = this.formOptionsService;
    this.formModel = _form;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.formModel.initStepper(this.stepper);
  }
  // todo: abstract this to a single place in code...
  nextStepWithDelay() {
    setTimeout(() => this.stepper.next(), 500);
  }
  selectionChange(event: any) {
    console.log(event);
  }
  addNewDriver() {
    this.formModel.drivers.push(this.formModel.newDriver);
  }
  addDriversPage() {
    this.addNewDriver();
    this.stepper.next();
  }
  test() {
    console.log("testing the drivers page");
  }
  addMore() {
    // This is super dodgy!! Need to find a much better way.
    this.formModel.newDriver = {};
    this.stepper.selectedIndex = this.formModel.addDriverIndex;
  }
}
