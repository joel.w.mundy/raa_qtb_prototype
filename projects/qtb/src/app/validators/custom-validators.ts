import { FormControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {
    static otherBusinessType(c: FormControl): ValidationErrors {
        const invalidBusinessTypes = [
            'taxi',
            'instructorOther',
            'instructorRaa',
            'food',
            'courtesy',
            'security'
        ];
        const value = c.value;
        const isValid = invalidBusinessTypes.indexOf(value) < 0;
        const message = {
            'businessType': {
                'message': 'disallowed business type'
            }
        };
        return isValid ? null : message;
    }
}