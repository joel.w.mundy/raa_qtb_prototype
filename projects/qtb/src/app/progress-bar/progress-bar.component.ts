import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'qtb-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() index: number;
  @Input() completed: number;
  @Input() formSteps: any;
  constructor() { }

  ngOnInit(): void {
  }
  progress(i): number {
    if (this.index < i) {
      return 0;
    }
    else if (this.index > i) {
      return 100;
    }
    else {
      return this.completed;
    }
  }
  sectionComplete(i): boolean {
    if (i < this.index) {
      return true;
    }
    if (i === this.index) {
      return true;
    }
    if (i > this.index) {
      return false;
    }
  }

}
