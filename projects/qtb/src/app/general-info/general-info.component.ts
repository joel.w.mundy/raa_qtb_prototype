import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { selectOption, QTB } from '../core/models/interfaces';
import { CarDetailsService } from '../car-details.service';
import { ControlContainer, NgForm, NgModelGroup, NgControl, NgModel, FormGroup } from '@angular/forms';
import { FormService } from '../form.service';
import { QuoteRequest, Driver, Vehicle } from '../core/models/quote-request.model';
import { MatStepper } from '@angular/material/stepper';
import { FormOptionsService } from '../form-options.service';

@Component({
  selector: 'qtb-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.scss'],
  // viewProviders: [{ provide: ControlContainer, useExisting: NgModelGroup }]
})
export class GeneralInfoComponent implements OnInit, AfterViewInit {
  
  options: FormOptionsService;
  @ViewChild('stepper') public stepper: MatStepper;
  @ViewChild('form') public form: NgForm;
  @ViewChild('matchedVehicleControl') public matchedVehicleControl: NgModel;
  @ViewChild('formSelectCar') public formSelectCar: NgForm;
  showInput: boolean = true;

  formModel: FormService;
  constructor(
    private formService: FormService,
    private formOptionsService: FormOptionsService
    ) {
      this.formModel = formService;
      this.options = this.formOptionsService;
  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.formService.initStepper(this.stepper);
    this.formService.formGroup = this.form;
  }
  test() {
    console.log('testing ... stuff');
  }
}
