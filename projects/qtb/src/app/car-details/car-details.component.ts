import { Component, OnInit } from '@angular/core';
import { selectOption, QTB } from '../core/models/interfaces';
import { CarDetailsService } from '../car-details.service';
import { FormService } from '../form.service';
import { ControlContainer, NgForm, NgModelGroup, NgModel } from '@angular/forms';
import { QuoteRequest, Vehicle } from '../core/models/quote-request.model';
import { FormOptionsService } from '../form-options.service';

@Component({
  selector: 'qtb-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]
})
export class CarDetailsComponent implements OnInit {

  formModel: FormService;
  carOptions: CarDetailsService;

  constructor(
    private carDetailsService: CarDetailsService,
    private formService: FormService) {
      this.carOptions = carDetailsService;
      this.formModel = formService;
    }

  ngOnInit(): void {
  }

}
