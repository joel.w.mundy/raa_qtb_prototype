import { Injectable, OnInit } from '@angular/core';
import { selectOption } from './core/models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CarDetailsService {
  defaultSelect = { value: 'select', display: 'select' };
  years: selectOption[];
  makes: selectOption[] = [
    this.defaultSelect,
    { value: 'ABARTH', display: 'ABARTH' },
    { value: 'ALFA ROMEO', display: 'ALFA ROMEO' },
    { value: 'ALPINE', display: 'ALPINE' },
    { value: 'ASTON MARTIN', display: 'ASTON MARTIN' },
    { value: 'AUDI', display: 'AUDI' },
    { value: 'BENTLEY', display: 'BENTLEY' },
    { value: 'BMW', display: 'BMW' },
    { value: 'BMW ALPINA', display: 'BMW ALPINA' },
    { value: 'CATERHAM', display: 'CATERHAM' },
    { value: 'CHEVROLET', display: 'CHEVROLET' },
    { value: 'CHRYSLER', display: 'CHRYSLER' },
    { value: 'CITROEN', display: 'CITROEN' },
    { value: 'FERRARI', display: 'FERRARI' },
    { value: 'FIAT', display: 'FIAT' },
    { value: 'FORD', display: 'FORD' },
    { value: 'FOTON', display: 'FOTON' },
    { value: 'GENESIS', display: 'GENESIS' },
    { value: 'GREAT WALL', display: 'GREAT WALL' },
    { value: 'HAVAL', display: 'HAVAL' },
    { value: 'HINO', display: 'HINO' },
    { value: 'HOLDEN', display: 'HOLDEN' },
    { value: 'HONDA', display: 'HONDA' },
    { value: 'HSV', display: 'HSV' },
    { value: 'HYUNDAI', display: 'HYUNDAI' },
    { value: 'INFINITI', display: 'INFINITI' },
    { value: 'INTERNATIONAL', display: 'INTERNATIONAL' },
    { value: 'ISUZU', display: 'ISUZU' },
    { value: 'IVECO', display: 'IVECO' },
    { value: 'JAGUAR', display: 'JAGUAR' },
    { value: 'JEEP', display: 'JEEP' },
    { value: 'KIA', display: 'KIA' },
    { value: 'LAMBORGHINI', display: 'LAMBORGHINI' },
    { value: 'LAND ROVER', display: 'LAND ROVER' },
    { value: 'LDV', display: 'LDV' },
    { value: 'LEXUS', display: 'LEXUS' },
    { value: 'LOTUS', display: 'LOTUS' },
    { value: 'MAHINDRA', display: 'MAHINDRA' },
    { value: 'MASERATI', display: 'MASERATI' },
    { value: 'MAZDA', display: 'MAZDA' },
    { value: 'McLAREN', display: 'McLAREN' },
    { value: 'MERCEDES-AMG', display: 'MERCEDES-AMG' },
    { value: 'MERCEDES-BENZ', display: 'MERCEDES-BENZ' },
    { value: 'MERCEDES-MAYBACH', display: 'MERCEDES-MAYBACH' },
    { value: 'MG', display: 'MG' },
    { value: 'MINI', display: 'MINI' },
    { value: 'MITSUBISHI', display: 'MITSUBISHI' },
    { value: 'MITSUBISHI FUSO', display: 'MITSUBISHI FUSO' },
    { value: 'MORGAN', display: 'MORGAN' },
    { value: 'NISSAN', display: 'NISSAN' },
    { value: 'PERFORMAX', display: 'PERFORMAX' },
    { value: 'PEUGEOT', display: 'PEUGEOT' },
    { value: 'PORSCHE', display: 'PORSCHE' },
    { value: 'RAM', display: 'RAM' },
    { value: 'RENAULT', display: 'RENAULT' },
    { value: 'ROLLS-ROYCE', display: 'ROLLS-ROYCE' },
    { value: 'SKODA', display: 'SKODA' },
    { value: 'SSANGYONG', display: 'SSANGYONG' },
    { value: 'SUBARU', display: 'SUBARU' },
    { value: 'SUZUKI', display: 'SUZUKI' },
    { value: 'TATA', display: 'TATA' },
    { value: 'TESLA', display: 'TESLA' },
    { value: 'TOYOTA', display: 'TOYOTA' },
    { value: 'UD', display: 'UD' },
    { value: 'VOLKSWAGEN', display: 'VOLKSWAGEN' },
    { value: 'VOLVO', display: 'VOLVO' }
  ];
  models: selectOption[] = [
    this.defaultSelect,
    { value: 'Berlinetta', display: 'Berlinetta' },
    { value: 'Modulo', display: 'Modulo' },
    { value: 'Mythos', display: 'Mythos' },
    { value: 'Rossa', display: 'Rossa' },
    { value: 'F80', display: 'F80' }
  ];
  transmissions: selectOption[] = [
    this.defaultSelect,
    { value: 'Manual', display: 'Manual' },
    { value: 'Automatic', display: 'Automatic' },
    { value: 'Automanual', display: 'Automanual' },
    { value: 'CVT', display: 'CVT' },
    { value: 'Other / Unknown', display: 'Other / Unknown' }
  ];
  engines: selectOption[] = [
    this.defaultSelect,
    { value: 'big', display: 'big' },
    { value: 'little', display: 'little' },
    { value: 'medium', display: 'medium' }
  ];

  initYears() {
    const minYear = 1960;
    const maxYear = new Date().getFullYear();
    let b = [...Array(maxYear - minYear + 1).keys()];
    let years = b.map((y, i) => {
      const mappedYear = (minYear + i).toString();
      return { value: mappedYear, display: mappedYear };
    });
    this.years = [this.defaultSelect, ...years];
  }
  constructor() {
    this.initYears();
  }
}
