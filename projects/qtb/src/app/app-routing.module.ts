import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DutyOfDisclosureComponent } from './duty-of-disclosure/duty-of-disclosure.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GeneralInfoComponent } from './general-info/general-info.component';
import { CarUseComponent } from './car-use/car-use.component';
import { QuoteComponent } from './quote/quote.component';
import { DriversComponent } from './drivers/drivers.component';
import { AppComponent } from './app.component';
import { ExperimentComponent } from './experiment/experiment.component';

const routes: Routes = [
  {
    path: 'qtb',
    component: AppComponent,
    children: [
      {path: '', redirectTo: 'duty-of-disclosure', pathMatch: 'full'},
      {path: 'duty-of-disclosure', component: DutyOfDisclosureComponent},
      {path: 'general-info', component: GeneralInfoComponent},
      {path: 'car-use', component: CarUseComponent},
      {path: 'drivers', component: DriversComponent},
      {path: 'your-quote', component: QuoteComponent},
      {path: 'experiment', component: ExperimentComponent},
      {path: '**', component: DutyOfDisclosureComponent}
    ]
  },
  {path: '**', redirectTo: '/qtb/duty-of-disclosure'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
